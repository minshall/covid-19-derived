* .csv files derived from the JHU data

this directory consists of a set of files derived from the JHU data.
some of the files are derived by the =../bin/coggregate= program, and
the rest by the =../bin/cowiden= program.

** a set of CSV files produced by =../bin/coggregate=:

- disaggregated.csv :: is basically the JHU data, with a =Date= column
  added (and cleaned up in some minor ways).  a major difference from
  the JHU data is that =Country_Region= is replaced with ISO 3166_1
  "iso3c" codes (e.g., "IRN" or "USA").
- province.csv :: aggregates =disaggregated.csv= "up" to the
  =Province_State= level.  (this is likely only of use, currently, in
  the US where =Province_State='s are broken into counties (=Admin2=);
  for all other geographic entities, the data in =province.csv= will
  (should!) be the same as that in =disaggregated.csv=.)
- country.csv :: data is aggregated to the country level.  i.e., all
  data relating to provinces/states within a country are aggregated
  together on a daily basis.  similar to the case with =province.csv=,
  for those countries that do *not* break data down to a
  =Province_State= level, the data in =country.csv= will be that same
  as that in =province.csv= and in =disaggregated=.
- world.csv :: data is aggregated to the world.  there is one row for
  each day reported in the data, with Country.Region set to "World"
- entire.csv :: all the above data, but in one [[https://gitlab.com/minshall/covid-19/-/issues/6][single file]].

** a set of .csv files produced by =../bin/cowiden=

for each of the files produced by =../bin/coggregate=, there is a
parallel file, with "wide-" prefixed to the filename, that reproduces
the data in the original file, and adds some columns that might be of
use.  please see the [[file:../bin/README.org][README]] file in =../bin/= for more information.

we also provide a set of "enhanced" .csv files, with columns not
included in the JHU distribution (but, probably, many JHU users
already create these, or similar, and certainly more, such columns).
these are the so-called "wide" files.  in addition to the normal JHU
columns (with our ISO_3166_1 Alpha_3 compliant names replacing -- XXX
was this a good idea, for our users? -- Country.Region), these provide
the following columns:
- Active :: the number of active cases (:=
  ==Confirmed-Deaths-Recovered==)
- Exited :: the number of people who were infected, but are no longer
  sick (:= ==Deaths+Recovered==).  (the serious student will note that
  we could define ==Active== := ==Confirmed-Active==.)
- Dtme ("DaysTillMoreExited") :: the number of days till ==Exited==
  exceeds the current ==Confirmed==.  this is *sort of* (*very* sort
  of) a measure of the duration of the disease.  (to really compute
  that, i can think of no way other than having case-level data.)
- Dtfa ("DaysTillFewerActive") :: the number of days till
    Confirmed-Recovered-Deaths (:= Active) is less than current
    Confirmed-Recovered-Deaths (:= current Active).  this basically
    says, how many days till we're more-or-less back to where we are
    today, in terms of number of active patients (*not*, hopefully, in
    terms of the slope).

** country names

one problem that people have seen with the "raw" JHU data are variant
country names.  to detect this, and slightly to avoid "political"
issues, i first map some non-conformant Country_Regions in the JHU
data into ISO 3166-1 country names, as used by the [[https://databank.worldbank.org/home][World Bank]] (see
[[file:fixups.sed][fixups.sed]]).

i then map the updated country names into ISO 3166-1 three letter
codes, which are then used for the rest of the processing, and used
when writing the .csv file.  these codes are included in the .csv
files in the column =Iso3c=.

errata: at least two names in the JHU data don't really have any
equivalent in the World Bank data: "Others" and "Cruise Ships" (which
are probably meant to represent the same "area", as "Others" appeared
(with cruise ships as Province.State) from 07.02.2020 until
10.03.2020, after which "Cruise Ships" appeared.  these names (with
spaces squashed out) are left in the file, and appear as the
Country.Region of those records.

** US county-level data

there seems to be a desire for US county-level data.  one can extract
something like ("exactly like"?) that from the output file
=disaggregated.csv=.  using dplyr in R:
#+begin_example
> filter(disaggregated, Country_Region=="USA", Admin2!="")
# A tibble: 23,935 x 13
   Date       FIPS  Admin2 Province_State Country_Region Last_Update        
   <date>     <chr> <chr>  <chr>          <chr>          <dttm>             
 1 2020-02-11 6073  San D… California     USA            2020-02-11 01:23:05
 2 2020-02-12 6073  San D… California     USA            2020-02-11 01:23:05
 3 2020-02-13 6073  San D… California     USA            2020-02-13 03:13:08
 4 2020-02-14 6073  San D… California     USA            2020-02-13 03:13:08
 5 2020-02-15 6073  San D… California     USA            2020-02-13 03:13:08
 6 2020-02-16 6073  San D… California     USA            2020-02-13 03:13:08
 7 2020-02-17 6073  San D… California     USA            2020-02-13 03:13:08
 8 2020-02-18 6073  San D… California     USA            2020-02-13 03:13:08
 9 2020-02-19 6073  San D… California     USA            2020-02-13 03:13:08
10 2020-02-20 6073  San D… California     USA            2020-02-13 03:13:08
# … with 23,925 more rows, and 7 more variables: Lat <dbl>, Long_ <dbl>,
#   Confirmed <dbl>, Deaths <dbl>, Recovered <dbl>, Active <dbl>,
#   Combined_Key <chr>
#+end_example
(one could say
: filter(disaggregated, Country_Region=="USA", FIPS!="")
but that would include state-level data, which might or might not be
desired.)

while i'm not an expert in JHU's timeseries .csv files, it seems that
using [[https://tidyr.tidyverse.org/index.html][tidyr's]] [[https://tidyr.tidyverse.org/articles/pivot.html][pivot_wider()]] command, we would come up with something
that might fit the bill for some people who would like to have time
series:
#+begin_src R :exports code
  timeseries <-
    pivot_wider(
      filter(disaggregated, Country_Region=="USA", Admin2!=""),
      c(FIPS,Admin2,Province_State,Country_Region),
      names_from = Date,
      values_from = Confirmed)
#+end_src
